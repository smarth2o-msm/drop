/* Copyright (C) Moonsubmarine Ltd, Inc
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * Written by Luca Galli <lgalli@moonsubmarine.com>, September 2015
 */

// Libraries import
var express = require('express');
var https = require('https');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var mainDomain = require('domain').create();
var fs = require('fs');
var passport = require('passport');

//Controllers import
var authController = require('./controllers/auth');
var playerController = require('./controllers/player');
var gameplayController = require('./controllers/gameplay');
var questionsController = require('./controllers/questions');
var configController = require ('./controllers/config');

//Models import
var User = require('./models/player');
var Game = require('./models/game');

//Import configuration
var configFile = require('./config');

// Create our Express application
var app = express();
var server = null;

var options = {
    key  : fs.readFileSync('./keys/server.key'),
    ca   : fs.readFileSync('./keys/server.csr'),
    cert : fs.readFileSync('./keys/server.crt')
};


//Defines server restart routines
mainDomain.on('error', function (err) {
    console.log("domain caught", err);
    console.log("Trying to restart in 5 seconds...");
    mongoose.connection.close();
    if(server)
        server.close();
    setTimeout(mainFunction, 5000);
});


var mainFunction = mainDomain.bind(function() {


    // Connect to the beerlocker MongoDB
    mongoose.connect('mongodb://localhost:27017/gamePlatform');

    // Use the body-parser package in our application
    app.use(bodyParser.urlencoded({
        extended: true
    }));


    console.log("Drop! Backend Framework v1.0");
    app.use(passport.initialize());

// Create our Express router
    var router = express.Router();

    router.route('/players')
        .post(playerController.postPlayer)
        .get(authController.isAuthenticated, require('permission')(), playerController.getPlayers);

    router.route('/players/login/:email')
        .get(authController.isAuthenticated, require('permission')(), playerController.loginPlayer);

    router.route('/players/:player_id')
        .get(authController.isAuthenticated, require('permission')(['player', 'admin']), playerController.getPlayer);

    router.route('/players/validate/:validate_token/:locale')
        .get(playerController.validate);


    //Binding gameplay controller
    router.route('/gameplay/action')
        .post(authController.isAuthenticated, require('permission')(['player', 'admin']), gameplayController.postAction);

    router.route('/gameplay/gamesession')
        .post(authController.isAuthenticated, require('permission')(['player', 'admin']), gameplayController.postGamesession);

    router.route('/gameplay/game')
        .post(authController.isAuthenticated, require('permission')(['admin']), gameplayController.postGame);

    router.route('/gameplay/gamesession/:session_id/close')
        .get(authController.isAuthenticated, require('permission')(['player', 'admin']), gameplayController.closeGamesession);


    //Binding questions controller
    router.route('/questions')
        .post(authController.isAuthenticated, require('permission')(['admin']),questionsController.postQuestion);

    router.route('/questions/:language/:difficulty')
        .get(authController.isAuthenticated, require('permission')(['player', 'admin']),questionsController.getQuestion);

    //Binding config controller
    router.route('/config/geaddress')
        .get(configController.geaddress);



// Register all our routes with /api
    app.use('/api', router);
    createAdmin();
    createGame();
    server = https.createServer(options, app).listen(configFile.defaultPort);
});


var createAdmin = function() {
    User.findOne({email: 'admin'}, function (err, found) {
        if (found) {
            console.log("Admin account credentials: email:" + found.email + " password: adminsh2o");
        }
        else {
            var user = new User({
                email: configFile.adminEmail,
                password: configFile.adminPassword,
                nickname: 'admin',
                role: 'admin',
                active: true
            });

            user.save(function (err) {
                if (err)
                    console.log("Error in creating admin user! " + err);
                else
                    console.log("Admin account created: email:" + user.email + " password:" + user.password);
            });
        }
    });

}

    var createGame = function() {
        Game.findOne({title: 'drop'}, function (err, found) {
            if (found) {
                console.log("Game instance found: " + found.title);
            }
            else {
                var game = new Game({
                    title: 'drop',
                    genre: 'boardgame',
                    minimumPlayers: 1,
                    maximumPlayers: 1
                });

                game.save(function (err) {
                    if (err)
                        console.log("Error in creating the Drop! game " + err);
                    else
                        console.log("Game instance created: " + game.title);
                });
            }
        });
    }


setTimeout(mainFunction, 100);
