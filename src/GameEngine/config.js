exports.geAddress = "http://131.175.141.236:8080/";
exports.adminEmail= 'admin';
exports.adminPassword= 'adminsh2o';
exports.defaultPort = 50666;
exports.mailIt = {
  from: 'validateaccount@smarth2o-fp7.eu',
  subject: 'Registrato nuovo account Drop!',
  text: 'Ricevi questa email perché tu (o qualcun altro) ha registrato un account per accedere a Drop!Game\n\n'+
        'Per piacere clicca sul link seguente per completare il processo di registrazione.\n\n'
};
exports.mailEn = {
  from: 'validateaccount@smarth2o-fp7.eu',
  subject: 'Newly registered Drop Account',
  text: 'You are receiving this because you (or someone else) have registered an account to access the Drop!Game.\n\n' +
    'Please click on the following link, or paste this into your browser to complete the process:\n\n'
};

exports.validationPageEn = '<html><body>User has been activated.</body>';
exports.validationPageIt = '<html><body>Utente attivato</body>';
