/* Copyright (C) Moonsubmarine Ltd, Inc
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * Written by Luca Galli <lgalli@moonsubmarine.com>, September 2015
 */

// Load required packages
var User = require('../models/player');
var Action = require('../models/action');
var Question = require('../models/question');
var Game = require('../models/game');

// Create endpoint /api/questions/ for POST
exports.postQuestion = function(req, res) {
    console.log(req.body);
    Game.findOne({ title: req.body.title }, function(err, retrievedGame) {
        if (err||retrievedGame==null) {
            console.log(retrievedGame);
            res.json({"error":"Cannot retrieve game or user or malformed parameters "+err});
        }
        else {
            var question = new Question({
                gameId: retrievedGame._id,
                question: req.body.question,
                answers: req.body.answers,
                solution: req.body.solution,
                difficulty: req.body.difficulty,
                region: req.body.region,
                language: req.body.language
            });

            question.save(function(err) {
                if (err)
                    res.send(err);

                res.json(question);
            });
        }
    });
};

// Create endpoint /api/questions/:language/:difficulty for GET
exports.getQuestion = function(req, res) {
    Question.find({ language: req.params.language, difficulty: req.params.difficulty }, function(err, retrievedQuestions) {
        if (err)
            res.send(err);
        else {
            res.send(retrievedQuestions);
        }
    });
};
