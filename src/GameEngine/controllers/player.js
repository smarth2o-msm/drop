/* Copyright (C) Moonsubmarine Ltd, Inc
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * Written by Luca Galli <lgalli@moonsubmarine.com>, September 2015
 */

// Load required packages
var User = require('../models/player');
var async = require('async');
var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');
var crypto = require('crypto');
//Import configuration
var configFile = require('../config');


// Create endpoint /api/players for POST
exports.postPlayer = function(req, res, next) {
  async.waterfall([
    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      User.findOne({ email: req.body.email }, function(err, found) {
        if (found) {
          res.json('The account is already registered');
        }
        else {
          console.log(req.body);
          var user = new User({
            email: req.body.email,
            password: req.body.password,
            nickname: req.body.nickname,
            role: req.body.role,
            name: req.body.surname,
            surname: req.body.surname,
            familyId: req.body.familyId,
            smartMeterId: req.body.smartMeterId,
            points: "0",
            type: "beginner",
            active: false,
            validateAccountToken: token,
            validateAccountExpires: Date.now() + 10800000 // 3 hours
          });

          user.save(function(err) {
            done(err, token, user);
          });
        }
      });
    },
    function(token, user, done) {

      var options = {
        auth: {
          api_user: 'LGMoonsubmarine',
          api_key: 'sendgridpassworddrop15'
        }
      };

      var client = nodemailer.createTransport(sgTransport(options));

      var mailOptions;

      switch(req.body.locale) {
        case "it": mailOptions = configFile.mailIt; console.log(configFile.mailIt); break;
        case "esp": break;
        case "en":
        default: mailOptions = configFile.mailEn; break;
      };



      mailOptions.text = mailOptions.text + '\n\n' +'https://' + req.headers.host + '/api/players/validate/' + token + '/'+req.body.locale+'\n\n';
      mailOptions.to = user.email;

      client.sendMail(mailOptions, function(err, info){
          if (err ){
            res.json(err);
          }
          else {
            res.json('Account with email:'+user.email+' has been registered. Validation required.');
          }
      });
    }
  ], function(err) {
    if (err) return next(err);
  });
};

// Create endpoint /api/players for GET
exports.getPlayers = function(req, res) {
  User.find(function(err, users) {
    if (err)
      res.send(err);

    res.json(users);
  });
};

// Create endpoint /api/players/validate/:validate_token for GET
exports.validate = function(req, res) {
  User.findOne({validateAccountToken: req.params.validate_token,  validateAccountExpires: { $gt: Date.now() } }, function(err, user) {
    if (err)
      res.send(err);
    if(null!=user) {
      user.active = true;
      user.validateAccountToken = "";
      user.validateAccountExpires = "";
      user.save(function(err) {
        if (err)
          res.send(err);
      switch(req.params.locale) {
        case "it": res.send(configFile.validationPageIt); break;
        case "en":
        default: res.send(configFile.validationPageEn); break;
      }
      });
    }
    else {
      res.json('Invalid token.');
    }
  });
};

// Create endpoint /api/players/:player_id for GET
exports.getPlayer = function(req, res) {
  User.findById({_id: req.params.player_id }, function(err, player) {
    if (err)
      res.send(err);
    res.json(player);
  });
};

// Create endpoint /api/players/:email for GET
exports.loginPlayer = function(req, res) {
    User.findOne({ email: req.params.email }, function(err, found) {
    if (err)
      res.send(err);
    res.json(found);
  });
};
