﻿/* Copyright (C) Moonsubmarine Ltd, Inc 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * Written by Luca Galli <lgalli@moonsubmarine.com>, September 2015
 */

using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Text;


public class QuestionHandler : MonoBehaviour {

	int reply = 0;
	public CanvasGroup current;
	public CanvasGroup correct;
	public CanvasGroup wrong;
	public Text correctPoints;
	public Text wrongPoints;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void fixDifficulty() {
		if (QuestionLoader.correctAnswers >= 0 && QuestionLoader.correctAnswers <= 1 && QuestionLoader.difficultyLevel != "easy") {
						QuestionLoader.difficultyLevel = "easy";
						QuestionLoader.questions = null;
				} else if (QuestionLoader.correctAnswers >= 2 && QuestionLoader.correctAnswers <= 3 && QuestionLoader.difficultyLevel != "medium") {
						QuestionLoader.difficultyLevel = "medium";
						QuestionLoader.questions = null;
				} else if (QuestionLoader.correctAnswers >= 4 && QuestionLoader.difficultyLevel != "hard") {
						QuestionLoader.difficultyLevel = "hard";
						QuestionLoader.questions = null;
				}
	}

	public void chosenAnswer(int number) {
		GameObject go = GameObject.Find ("Question");
		QuestionLoader questionLoader = go.GetComponent <QuestionLoader> ();
		reply = questionLoader.getReply ();
		GlobalUtils.enableCanvas(current,false);
		GameController instance = GameController.get ();
		if (number.Equals (reply)) {
			if (GameController.isAuthenticated) {
				instance.addPointsAction(10,QuestionLoader.questionId);
			}
			GameController.totalPoints+=10;
			QuestionLoader.correctAnswers+=1;
			fixDifficulty();
			correctPoints.text = GameController.totalPoints.ToString();
			GlobalUtils.enableCanvas(correct,true);
			GlobalUtils.enableCanvas(wrong,false);

			GameController.addActionGe();
		}
		else {
			if (GameController.isAuthenticated) {
				instance.addPointsAction(0,QuestionLoader.questionId);
			}
			QuestionLoader.correctAnswers-=1;
			fixDifficulty();
			wrongPoints.text = GameController.totalPoints.ToString();
			GlobalUtils.enableCanvas(correct,false);
			GlobalUtils.enableCanvas(wrong,true);
		}
		questionLoader.newQuestion ();
	}


}
