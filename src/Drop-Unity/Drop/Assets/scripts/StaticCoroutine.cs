﻿/* Copyright (C) Moonsubmarine Ltd, Inc 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * Written by Luca Galli <lgalli@moonsubmarine.com>, September 2015
 */

using UnityEngine;
using System;
using System.Collections;

public class StaticCoroutine : MonoBehaviour {
	
	private static StaticCoroutine mInstance = null;
	
	private static StaticCoroutine instance
	{
		get
		{
			if (mInstance == null)
			{
				mInstance = GameObject.FindObjectOfType(typeof(StaticCoroutine)) as StaticCoroutine;
				
				if (mInstance == null)
				{
					mInstance = new GameObject("StaticCoroutine").AddComponent<StaticCoroutine>();
				}
			}
			return mInstance;
		}
	}
	
	void Awake()
	{
		if (mInstance == null)
		{
			mInstance = this as StaticCoroutine;
		}
	}
	
	IEnumerator Perform(IEnumerator coroutine)
	{
		yield return StartCoroutine(coroutine);
		Die();
	}
	
	/// <summary>
	/// Place your lovely static IEnumerator in here and witness magic!
	/// </summary>
	/// <param name="coroutine">Static IEnumerator</param>
	public static void DoCoroutine(IEnumerator coroutine)
	{
		instance.StartCoroutine(instance.Perform(coroutine)); //this will launch the coroutine on our instance
	}

	void Die()
	{
		mInstance = null;
		Destroy(gameObject);
	}
	
	void OnApplicationQuit()
	{
		mInstance = null;
	}
}