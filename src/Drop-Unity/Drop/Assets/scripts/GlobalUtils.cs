﻿/* Copyright (C) Moonsubmarine Ltd, Inc 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * Written by Luca Galli <lgalli@moonsubmarine.com>, September 2015
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalUtils : MonoBehaviour {
	
	public CanvasGroup previous,next;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	private static System.Random rng = new System.Random();  
	
	public static IList<T> Shuffle<T>(IList<T> list)  
	{  
		int n = list.Count;  
		while (n > 1) {  
			n--;  
			int k = rng.Next(n + 1);
			
			T value = list[k];  
			list[k] = list[n];  
			list[n] = value;  
		}  
		return list;
	}
	
	
	public void nextMenu() {
		if ((previous) && (next)) {
			enableCanvas (previous, false);
			enableCanvas (next, true);
		}
	}
	
	public void quit() 
	{
		GameController instance = GameController.get ();
		instance.quitGame ();
		Application.Quit ();
	}
	
	public static void enableCanvas(CanvasGroup chosen,bool value) {
		if (value) {
			chosen.interactable = true;
			chosen.alpha = 1;
			chosen.blocksRaycasts = true;
		} else {
			chosen.interactable = false;
			chosen.alpha = 0;
			chosen.blocksRaycasts = false;
		}
	}
}
