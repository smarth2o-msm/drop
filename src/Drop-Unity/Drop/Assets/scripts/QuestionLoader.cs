﻿/* Copyright (C) Moonsubmarine Ltd, Inc 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * Written by Luca Galli <lgalli@moonsubmarine.com>, September 2015
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;

public class QuestionLoader : MonoBehaviour {
	
	public static string difficultyLevel = "easy";
	public static int correctAnswers = 0;
	public static IList<object> questions = null;

	public static string questionId = "";
	
	public Text question;
	public Text answer1;
	public Text answer2;
	public Text answer3;
	public Text answer4;
	int reply = 0;
	
	
	// Use this for initialization
	void Start () {
		newQuestion ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void newQuestion () {
		//If the network is not available fallback to the questions from the database
		if (!GameController.inetAvail) {
			loadFromInternalDb();
		} else {
			GameController.get ().StartCoroutine(loadQuestionRoutine());
		}
	}
	
	private void loadFromInternalDb() {
		SqliteDatabase sqlDB = new SqliteDatabase ("questions.db");
		string query = "";
		if (Application.systemLanguage.ToString ().Equals ("Italian"))
			query = "SELECT * FROM DOMANDE";
		else
			query = "SELECT * FROM QUESTIONS";
		DataTable retrieved = sqlDB.ExecuteQuery (query);
		if (retrieved.Rows.Count > 0) {
			int row = Random.Range (1, retrieved.Rows.Count) - 1;
			questionId = "LocalDB";
			question.text = retrieved.Rows [row] [retrieved.Columns [0].ToString ()].ToString ();
			answer1.text = retrieved.Rows [row] [retrieved.Columns [1].ToString ()].ToString ();
			answer2.text = retrieved.Rows [row] [retrieved.Columns [2].ToString ()].ToString ();
			answer3.text = retrieved.Rows [row] [retrieved.Columns [3].ToString ()].ToString ();
			answer4.text = retrieved.Rows [row] [retrieved.Columns [4].ToString ()].ToString ();
			reply = (int)retrieved.Rows [row] [retrieved.Columns [5].ToString ()];
		}
	}
	
	private IEnumerator loadQuestionRoutine() {
		if (null == questions && GameController.inetAvail) {
			//Recover the question from the DB
			WWWForm form = new WWWForm ();
			Dictionary<string,string> headers = form.headers;
			headers ["Authorization"] = "Basic " + System.Convert.ToBase64String (
				System.Text.Encoding.ASCII.GetBytes (GameController.getAdmin () + ":" + GameController.getAdminPass ()));
			
			string languageCode = "en";
			//We have just two language so far, just check over the Italian field
			if (Application.systemLanguage.ToString ().Equals ("Italian"))
				languageCode = "it";
			else
				languageCode = "en";
			
			
			WWW www = new WWW (GameController.basePath + "/questions/" + languageCode + "/" + difficultyLevel, null, headers);
			
			float elapsedTime = 0.0f;
			
			while (!www.isDone) {
				elapsedTime += Time.deltaTime;
				
				if (elapsedTime >= 10.0f)
					break;
				yield return null; 
			}
			
			
			if (!www.isDone || !string.IsNullOrEmpty (www.error)) {
				Debug.LogError (string.Format ("Failed to retrieve the question!\n{0}", www.error));
				loadFromInternalDb ();
			} else {
				string response = www.text;
				
				if (GameController.debug)
					Debug.Log (elapsedTime + " : " + response);    
				
				questions = (IList<object>)Json.Deserialize (response);
				questions = GlobalUtils.Shuffle(questions);
			}
		}
		if (questions != null && questions.Count < 1) {
			loadFromInternalDb ();
		} else if (questions != null) {
			int row = Random.Range (0, questions.Count);
			IDictionary retrieved = (IDictionary)questions [row];
			questionId = "Online_"+(string)retrieved ["_id"];
			question.text = (string)retrieved ["question"]; 
			answer1.text = (string)((IList)retrieved ["answers"]) [0];
			answer2.text = (string)((IList)retrieved ["answers"]) [1];
			answer3.text = (string)((IList)retrieved ["answers"]) [2];
			answer4.text = (string)((IList)retrieved ["answers"]) [3];
			reply = int.Parse ((string)retrieved ["solution"]);
			//Remove the current question to ensure fresh responses
			questions.RemoveAt(row);
		}
	}
	
	
	public int getReply() {
		return reply;
	}
}
