﻿/* Copyright (C) Moonsubmarine Ltd, Inc 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * Written by Luca Galli <lgalli@moonsubmarine.com>, September 2015
 */

using UnityEngine;
using System.Collections;
using System;
using System.Text;
using System.Collections.Generic;
using SmartLocalization;
using System.Net.NetworkInformation;
using UnityEngine.UI;
using MiniJSON;
using System.Text.RegularExpressions;

public class GameController : MonoBehaviour {
	//Put it to true to skip the call of the Android plugins in unity
	public static bool isUnity = false;
	//Put it to true to enable debug messages
	public static bool debug = true;
	
	public static bool inetAvail;
	
	public static bool isAuthenticated;
	public static bool isSkipped;

	public static string authNickname = "";
	
	public Text connection;
	public GameObject wifiStatus;
	public GameObject logout;
	public Text invalidUsername;
	public InputField username;
	public InputField password;
	
	public InputField registrationUsername;
	public InputField registrationPassword;
	public InputField doublePassword;
	public InputField nickname;
	public Text registrationError;

	public Text nicknameField;
	public Text errorUI;
	
	public CanvasGroup prev;
	public CanvasGroup next;
	public CanvasGroup net;
	public CanvasGroup message;
	public Text messageText;
	
	public static CanvasGroup previous;
	public static CanvasGroup nextStd;
	public static CanvasGroup nextNet = null;
	

	
	public GameObject registrationButton;
	
	public static int totalPoints = 0;
	
	private static DropCodeScanner scanner;
	
	public static string chosenEmail=null;
	public static string chosenPassword=null;
	
	private static string currentSessionId = null;
	
	public static string serverAddress = "www.smarth2o.ch:5066";
	//public static string serverAddress = "localhost";
	
	public static String basePath = "https://"+serverAddress+"/api";
	
	private static string geBasePath = "";
	
	private static string geActionPath = "community/UserActivityCreditWebServiceREST/AssignActionsToUsersByMail/assignActionsToUsers";
	
	private static GameController mInstance = null;
	
	private bool mInitialized = false;
	
	private static string adminUser = "admin";
	
	private static string adminPassword = "adminsh2o";
	
	
	private static GameController instance
	{
		get
		{
			if (mInstance == null)
			{
				mInstance = GameObject.FindObjectOfType(typeof(GameController)) as GameController;
				
				if (mInstance == null)
				{
					mInstance = new GameObject("GameController").AddComponent<GameController>();
				}
			}
			return mInstance;
		}
	}
	
	
	void Awake()
	{
		string cert1 = "-----BEGIN CERTIFICATE-----\n" +
			"MIIFJjCCBA6gAwIBAgIIANIYzG+NrEIwDQYJKoZIhvcNAQELBQAwgbQxCzAJBgNV\n"+
				"BAYTAlVTMRAwDgYDVQQIEwdBcml6b25hMRMwEQYDVQQHEwpTY290dHNkYWxlMRow\n"+
				"GAYDVQQKExFHb0RhZGR5LmNvbSwgSW5jLjEtMCsGA1UECxMkaHR0cDovL2NlcnRz\n"+
				"LmdvZGFkZHkuY29tL3JlcG9zaXRvcnkvMTMwMQYDVQQDEypHbyBEYWRkeSBTZWN1\n"+
				"cmUgQ2VydGlmaWNhdGUgQXV0aG9yaXR5IC0gRzIwHhcNMTUwOTIyMDcxNTM4WhcN\n"+
				"MTYwOTIyMDcxNTM4WjA9MSEwHwYDVQQLExhEb21haW4gQ29udHJvbCBWYWxpZGF0\n"+
				"ZWQxGDAWBgNVBAMTD3d3dy5zbWFydGgyby5jaDCCASIwDQYJKoZIhvcNAQEBBQAD\n"+
				"ggEPADCCAQoCggEBAO5YpFN1E6Wyz7f8PmURNrPnlyA3j1N2vlZhczqzRGXrdbuI\n"+
				"WDn8Wkj1CY+p8m2SF936MvpACNVllFxP2SbYTJLBTjFcld7p4OyEzCUorj5AiAtN\n"+
				"z+zT/X5XUd8pvfRPpyR1/aWPJGtUJEerqRsC6Vc8wqmzz5/PpRk18Or977Dk2juf\n"+
				"hrd1FTyfMuj/7+qmCjuQdUhcxKgOhvuDJZkhqKWZG0wAQbPjnrCFdeo2meXrRp/E\n"+
				"LDyVC5cKCa1PoOd444lba6S0unMDaz7ePWvELB9EFA+5PCo9vNmJgBiLhq4/+/Vq\n"+
				"P6cL18g7sX10vhxjA3fCKYS86+lIbbolMRH1lpcCAwEAAaOCAbAwggGsMAwGA1Ud\n"+
				"EwEB/wQCMAAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMA4GA1UdDwEB\n"+
				"/wQEAwIFoDA3BgNVHR8EMDAuMCygKqAohiZodHRwOi8vY3JsLmdvZGFkZHkuY29t\n"+
				"L2dkaWcyczEtMTI2LmNybDBTBgNVHSAETDBKMEgGC2CGSAGG/W0BBxcBMDkwNwYI\n"+
				"KwYBBQUHAgEWK2h0dHA6Ly9jZXJ0aWZpY2F0ZXMuZ29kYWRkeS5jb20vcmVwb3Np\n"+
				"dG9yeS8wdgYIKwYBBQUHAQEEajBoMCQGCCsGAQUFBzABhhhodHRwOi8vb2NzcC5n\n"+
				"b2RhZGR5LmNvbS8wQAYIKwYBBQUHMAKGNGh0dHA6Ly9jZXJ0aWZpY2F0ZXMuZ29k\n"+
				"YWRkeS5jb20vcmVwb3NpdG9yeS9nZGlnMi5jcnQwHwYDVR0jBBgwFoAUQMK9J47M\n"+
				"NIMwojPX+2yz8LQsgM4wJwYDVR0RBCAwHoIPd3d3LnNtYXJ0aDJvLmNoggtzbWFy\n"+
				"dGgyby5jaDAdBgNVHQ4EFgQU1k2BhDCiuwwbZonGjLHU93PMFk8wDQYJKoZIhvcN\n"+
				"AQELBQADggEBAC8IhejEcdUUiGtIgoWNrN90i5GUR7g9YyCMXMPAyokE7J0YLeB8\n"+
				"+c5LblBWxwlfs4UfhOaSSgWX0g5ynV4ap56EofxdicJXU6dqj5/iJy7i0ZxIOhEl\n"+
				"pddfl2JZFKuBCixQLJV3SgItoO636OLKZPivlt/nlB7d88OEj77prcc+BD85njBM\n"+
				"4ULi1sNm40RVSRxBcDFLMdLpj/InQvhc6HUSTvs9p+QSBwgY+i3lqvm+7ieuLopE\n"+
				"pZSIP0x8jDjcQLr/xngIWhYOUtW0tVljfexQYsChJwO9ZrhRMDnVE+Zr0rfEmNgT\n"+
				"wfZNmKYXZFMdz6YhIyG1IFwjdEP240r2ueo=\n"+
				"-----END CERTIFICATE-----";
		
		
		AndroidHttpsHelper.AddCertificate(cert1);
		mInitialized = true;
		
		if (mInstance == null)
		{
			mInstance = this as GameController;
		}
	}
	
	
	// Use this for initialization
	void Start () {
		Debug.Log (Application.systemLanguage.ToString ());
		LanguageManager languageManager = LanguageManager.Instance;
		languageManager.defaultLanguage = "en";
		if (Application.systemLanguage.ToString () == "Italian")
			languageManager.ChangeLanguage ("it");
		
		if(mInitialized == false)
		{
			Debug.LogError("Initialization failed. Default WWW class is used.");
		}
		previous = prev;
		nextStd = next;
		nextNet = net;
		isAuthenticated = false;
		isSkipped = false;
		scanner = GetComponent<DropCodeScanner>();
	}
	
	
	public void loginUser() {
		if(!(password.Equals("")||username.Equals("")))
			instance.StartCoroutine(loginUserRoutine(username.text, password.text));
	}
	
	
	public void registerUser() {
		if (!(registrationPassword.Equals ("") || registrationUsername.Equals ("") || (!registrationPassword.text.Equals (doublePassword.text)) || nickname.Equals ("")))
			instance.StartCoroutine (registrationUserRoutine (registrationUsername.text, registrationPassword.text, nickname.text, LanguageManager.Instance.LoadedLanguage));
		else if (registrationPassword.Equals ("")) {
			if (!LanguageManager.Instance.LoadedLanguage.Equals ("it"))
				registrationError.text = "Password required!";
			else
				registrationError.text = "Password richiesta!";
		} else if (registrationUsername.Equals ("")) {
			if (!LanguageManager.Instance.LoadedLanguage.Equals ("it"))
				registrationError.text = "Password required!";
			else
				registrationError.text = "Password richiesta!";
		} else if (!registrationPassword.text.Equals (doublePassword.text)) {
			if (!LanguageManager.Instance.LoadedLanguage.Equals ("it"))
				registrationError.text = "The passwords do not match!";
			else
				registrationError.text = "Le password sono diverse!";
		} else if (nickname.Equals ("")) {
			if (!LanguageManager.Instance.LoadedLanguage.Equals ("it"))
				registrationError.text = "Nickname required!";
			else
				registrationError.text = "Nickname richiesto!";
		}
	}
	
	public void quitGame() {
		if (isAuthenticated) {
			instance.StartCoroutine (cgpsRoutine (currentSessionId));
			currentSessionId = null;
		}
	}
	
	
	IEnumerator registrationUserRoutine(String username, String password, String nickname, String locale) {
		registrationError.text = "";
		errorUI.text = "";
		WWWForm form = new WWWForm();
		Dictionary<string,string> headers = form.headers;
		username = username.Trim ();
		username = username.ToLower ();
		password = password.Trim ();
		nickname = nickname.Trim ();
		form.AddField ("email", username);
		form.AddField ("password", password);
		form.AddField ("nickname", nickname);
		form.AddField ("role", "player");
		form.AddField ("locale", locale);
		byte[] rawData = form.data;
		
		if(!LanguageManager.Instance.LoadedLanguage.Equals("it"))
			errorUI.text = "Registering...";
		else
			errorUI.text = "Registrazione...";
		
		
		WWW www = new WWW(basePath+"/players/", rawData, headers);
		
		float elapsedTime = 0.0f;
		
		while (!www.isDone) {
			elapsedTime += Time.deltaTime;
			
			if (elapsedTime >= 10.0f) break;
			
			yield return null;  
		}
		
		
		if (!www.isDone || !string.IsNullOrEmpty (www.error)) {
			Debug.LogError (string.Format ("Failed Register call!\n{0}", www.error));
			if(!LanguageManager.Instance.LoadedLanguage.Equals("it")) 
				registrationError.text = "Error in the registration, try later!";
			else 
				registrationError.text = "Errore di registrazione, riprova più tardi!";
		} else {
			string response = www.text;
			if(response!="\"The account is already registered\"") {
				GlobalUtils utils = registrationButton.GetComponent<GlobalUtils>();
				addRegistrationGe(username);
				utils.nextMenu();
			}
			else {
				if(!LanguageManager.Instance.LoadedLanguage.Equals("it"))
					registrationError.text = "Email already registered!";
				else
					registrationError.text = "Email già utilizzata!";
			}
		}
		errorUI.text = "";
		yield return null;
	}
	
	
	IEnumerator loginUserRoutine(String username, String password) {
		invalidUsername.text = "";
		nicknameField.text = "";
		errorUI.text = "";
		if (loggedIn ()) {
			yield return instance.StartCoroutine (ogpsRoutine ());
			scanner.scanCode ();
				}
		else
		{
						Dictionary<string,string> headers = new Dictionary<string,string> ();
						headers.Add ("Content-Type", "application/json");
						username = username.Trim ();
						username = username.ToLower ();
						password = password.Trim ();
						headers ["Authorization"] = "Basic " + System.Convert.ToBase64String (
						System.Text.Encoding.ASCII.GetBytes (username + ":" + password));
			
						if (!LanguageManager.Instance.LoadedLanguage.Equals ("it"))
								errorUI.text = "Authenticating...";
						else
								errorUI.text = "Autenticazione...";

						WWW www = new WWW (basePath + "/players/login/" + username, null, headers);
			
						float elapsedTime = 0.0f;
			
						while (!www.isDone) {
								elapsedTime += Time.deltaTime;
				
								if (elapsedTime >= 10.0f)
										break;
				
								yield return null;  
						}
			
						if (!www.isDone || !string.IsNullOrEmpty (www.error)) {
								Debug.LogError (string.Format ("Failed Login call!\n{0}", www.error));
								if (!LanguageManager.Instance.LoadedLanguage.Equals ("it"))
										invalidUsername.text = "Invalid username or password!";
								else
										invalidUsername.text = "Credenziali non valide!";
						} else {
								invalidUsername.text = "";
								string response = www.text;
				
								if (GameController.debug)
										Debug.Log (elapsedTime + " : " + response);    
				
								IDictionary search = (IDictionary)Json.Deserialize (response);
				
								authNickname = (string)search ["nickname"];
								nicknameField.text = authNickname;
				
								chosenEmail = username;
								chosenPassword = password;
								if (GameController.debug)
										Debug.Log ("Retrieved nickname is: " + authNickname);
								isAuthenticated = true;
								logout.SetActive(true);
								if (null == scanner)
										scanner = GetComponent<DropCodeScanner> ();
								scanner.previous = previous;
								scanner.nextNet = next;
								scanner.nextStd = next;
								scanner.error = message;
								scanner.errorString = messageText;
								registerLogin (authNickname, chosenEmail, chosenPassword);
								//Open a game session since we are online
								yield return instance.StartCoroutine (ogpsRoutine ());
								scanner.scanCode ();
						}
				}
		errorUI.text = "";
		yield return null;
	}

	public bool loggedIn() {
		SqliteDatabase sqlDB = new SqliteDatabase ("questions.db");
		string query = "SELECT * FROM LOGINS";
		DataTable retrieved = sqlDB.ExecuteQuery (query);
		if (retrieved.Rows.Count > 0) {
			int row = UnityEngine.Random.Range (1, retrieved.Rows.Count) - 1;
			string loggedIn = retrieved.Rows [row] [retrieved.Columns [0].ToString ()].ToString ();
			string foundEmail = retrieved.Rows [row] [retrieved.Columns [1].ToString ()].ToString ();
			string foundPassword = retrieved.Rows [row] [retrieved.Columns [2].ToString ()].ToString ();
			setAuth(loggedIn,foundEmail, foundPassword);
			Debug.Log("User already authenticated, using database auth credentials: "+foundEmail+" pass:"+foundPassword);
			logout.SetActive(true);
			//Skip the authentication menu and just go to the question phase
			scanner.previous = previous;
			scanner.nextNet = next;
			scanner.nextStd = next;
			return true;
		}
		return false;
	}

	//Register the player login into the DB to avoid multiple authentications on the same device
	private void registerLogin(string nickname, string email,string password) {
		SqliteDatabase sqlDB = new SqliteDatabase ("questions.db");
		string query = "INSERT INTO LOGINS (Nickname,Email,Pass) VALUES (\'" + nickname + "\',\'" + email + "\',\'"+password+"\')";
		sqlDB.ExecuteNonQuery (query);
	}

	//Delete the logins in the database to ask for a new authentication
	public void deleteLogin() {
		SqliteDatabase sqlDB = new SqliteDatabase ("questions.db");
		string query = "DELETE FROM LOGINS";
		sqlDB.ExecuteNonQuery (query);
		isAuthenticated = false;
		chosenEmail = null;
		chosenPassword = null;
		nicknameField.text = "";
		logout.SetActive (false);
		if (null == scanner)
			scanner = GetComponent<DropCodeScanner> ();
		scanner.previous = previous;
		scanner.nextNet = net;
		scanner.nextStd = next;
		totalPoints = 0;
		username.text = "";
		password.text = "";
		Debug.Log ("User logged out");
	}

	public void setAuth(string nickname, string email, string pass) {
		authNickname = nickname;
		nicknameField.text = authNickname;
		
		chosenEmail = email;
		chosenPassword = pass;
		isAuthenticated = true;
		logout.SetActive (true);
		if(null==scanner)
			scanner = GetComponent<DropCodeScanner>();
		scanner.previous = previous;
		scanner.nextNet = next;
		scanner.nextStd = next;
		scanner.error = message;
		scanner.errorString = messageText;
	}


	public void chooseRightOnline() {
		isUnity = false;
		if(null==scanner)
			scanner = GetComponent<DropCodeScanner>();
		if(loggedIn())
			scanner.scanCode();
		else
			scanner.nextMenu();
	}
	
	
	public void openGameplaySession() {
		instance.StartCoroutine(ogpsRoutine());
	}
	

	
	//Open a gameSession for Drop!. The id is stored in the currentSessionID variable.
	IEnumerator ogpsRoutine() {
		
		WWWForm form = new WWWForm();
		Dictionary<string,string> headers = form.headers;
		
		headers["Authorization"] = "Basic " + System.Convert.ToBase64String(
			System.Text.Encoding.ASCII.GetBytes(chosenEmail+":"+chosenPassword));
		form.AddField ("email", chosenEmail);
		form.AddField ("title", "drop");
		byte[] rawData = form.data;
		
		
		
		
		WWW www = new WWW(basePath+"/gameplay/gamesession/", rawData, headers);
		
		float elapsedTime = 0.0f;
		
		while (!www.isDone) {
			elapsedTime += Time.deltaTime;
			
			if (elapsedTime >= 10.0f) break;
			
			yield return null;  
		}
		
		if (!www.isDone || !string.IsNullOrEmpty (www.error)) {
			Debug.LogError (string.Format ("Failed Gamesession Open call!\n{0}", www.error));
			yield break;
		} else {
			
			string response = www.text;
			
			if(GameController.debug)
				Debug.Log (elapsedTime + " : " + response);    
			
			IDictionary search = (IDictionary)Json.Deserialize (response);
			
			if(GameController.debug)
				Debug.Log ("Retrieved gamesession:" + search);
			
			currentSessionId = (string)search ["_id"];
			
			yield return currentSessionId;
			
		}
		
	}
	
	public void closeGameplaySession(string sessionID) {
		instance.StartCoroutine(cgpsRoutine(sessionID));
	}
	
	//Closes a gameplaySession with id specified
	IEnumerator cgpsRoutine(string sessionID) {
		WWWForm form = new WWWForm();
		Dictionary<string,string> headers = form.headers;
		
		sessionID=sessionID.Replace ("ObjectId\"", "");
		sessionID=sessionID.Replace ("\")", "");
		
		headers["Authorization"] = "Basic " + System.Convert.ToBase64String(
			System.Text.Encoding.ASCII.GetBytes(chosenEmail+":"+chosenPassword));
		string url = basePath+"/gameplay/gamesession/"+sessionID+"/close";
		WWW www = GET (url, headers);
		float elapsedTime = 0.0f;
		
		while (!www.isDone) {
			elapsedTime += Time.deltaTime;
			
			if (elapsedTime >= 10.0f) break;
			
			yield return null;  
		}
		
		if (!www.isDone || !string.IsNullOrEmpty (www.error) || sessionID.Equals("")) {
			Debug.LogError (string.Format ("Failed Gamesession Close call!\n{0}", www.error));
			yield break;
		} else {
			
			string response = www.text;
			
			if(GameController.debug)
				Debug.Log (elapsedTime + " : " + response);    
			
			IDictionary search = (IDictionary)Json.Deserialize (response);
			
			sessionID = "";
			
			if(GameController.debug)
				Debug.Log ("Closed gamesession:" + search);
		}
	}
	
	public static IEnumerator WaitForRequest(WWW www)
	{
		yield return www;
		
		// check for errors
		if (www.error == null)
		{
			if(GameController.debug)
				Debug.Log("WWW Ok!: " + www.text);
		} else {
			Debug.LogError("WWW Error: "+ www.error);
		}    
	}
	
	public static void retrieveGePath() {
		StaticCoroutine.DoCoroutine (rgpRoutine ());
	}
	
	public static IEnumerator rgpRoutine() {
		WWWForm form = new WWWForm();
		Dictionary<string,string> headers = form.headers;
		
		string url = basePath+"/config/geaddress";
		WWW www = GET (url, headers);
		float elapsedTime = 0.0f;
		
		while (!www.isDone) {
			elapsedTime += Time.deltaTime;
			
			if (elapsedTime >= 10.0f) break;
			
			yield return null;  
		}
		
		if (!www.isDone || !string.IsNullOrEmpty (www.error)) {
			Debug.LogError (string.Format ("Failed to retrieve the Gamification Engine Address!\n{0}", www.error));
			yield break;
		} else {
			
			string response = www.text;
			
			if(GameController.debug)
				Debug.Log (elapsedTime + " : " + response);    
			
			string geAddress = (string) Json.Deserialize (response);
			
			if(GameController.debug)
				Debug.Log ("Retrieved Gamification Engine:" + geAddress);
			
			geBasePath = geAddress.ToString();
		}
	}
	
	
	public void addPointsAction(int points, string questionId) {
		instance.StartCoroutine(apaRoutine(points, questionId, true));
	}
	
	public static GameController get() {
		return instance;
	}
	
	IEnumerator apaRoutine(int points,string questionId,bool openClose) {
		
		if (openClose) {
			if (currentSessionId != null) {
				WWWForm form = new WWWForm ();
				Dictionary<string,string> headers = form.headers;
				
				headers ["Authorization"] = "Basic " + System.Convert.ToBase64String (
					System.Text.Encoding.ASCII.GetBytes (chosenEmail + ":" + chosenPassword));
				form.AddField ("gameplayId", currentSessionId);
				form.AddField ("description", "Adding points for quiz submission");
				form.AddField ("points", points);
				form.AddField ("questionId", questionId);
				byte[] rawData = form.data;
				
				
				WWW www = new WWW (basePath + "/gameplay/action/", rawData, headers);
				
				float elapsedTime = 0.0f;
				
				while (!www.isDone) {
					elapsedTime += Time.deltaTime;
					
					if (elapsedTime >= 10.0f)
						break;
					
					yield return null;  
				}
				
				IDictionary retrievedAction;
				if (!www.isDone || !string.IsNullOrEmpty (www.error)) {
					Debug.LogError (string.Format ("Failed to submit points!\n{0}", www.error));
					yield break;
				} else {
					
					string response = www.text;
					
					if(GameController.debug)
						Debug.Log (elapsedTime + " : " + response);    
					
					retrievedAction = (IDictionary)Json.Deserialize (response);
					
					if(GameController.debug)
						Debug.Log ("Retrieved action:" + retrievedAction);
				}
				yield return retrievedAction;
			}
			
		} else {
			throw new System.ApplicationException("Feature currently not supported");
		}
	}
	
	public void skip() {
		isSkipped = true;
		DropCodeScanner scanner = GetComponent<DropCodeScanner> ();
		scanner.previous = previous;
		scanner.nextNet = next;
		scanner.nextStd = next;
		scanner.error = message;
		scanner.errorString = messageText;
		scanner.scanCode();
		Debug.Log ("Skipping authentication for the whole session");
	}
	
	
	
	// Update is called once per frame
	void Update () {
		ConnectionTesterStatus status = Network.TestConnection();
		switch (status.ToString()) {
			case "Error":
			case "Undetermined":inetAvail=false;break;
			case "PublicIPIsConnectable":
			case "PublicIPPortBlocked":
			case "PublicIPNoServerStarted":
			case "LimitedNATPunchthroughPortRestricted":
			case "LimitedNATPunchthroughSymmetric":
			case "NATpunchthroughFullCone":
			case "NATpunchthroughAddressRestrictedCone":inetAvail=true;break;
			default:inetAvail=false;break;
		}
		connection.text = "Connection: " + status.ToString();
		if (inetAvail) {
			wifiStatus.SetActive (true);
			if ("" == geBasePath && inetAvail) {
				geBasePath="retrieving";
				loggedIn();
				retrieveGePath ();
			}
		}
		else
			wifiStatus.SetActive (false);
		if (Input.GetKeyDown(KeyCode.Escape)) { Application.Quit(); }
	}
	
	
	
	public static WWW GET(string url, Dictionary<string,string> headers)
	{
		
		WWW www = new WWW (url,null, headers);
		StaticCoroutine.DoCoroutine(WaitForRequest (www));
		return www; 
	}
	
	

	public static void addActionGe () {
		if ("retrieving" == geBasePath && inetAvail) {
			retrieveGePath();
		}
		else
		if (chosenEmail != null && chosenEmail != "") {
			long currentTs = (long)(DateTime.UtcNow - new DateTime (1970, 1, 1)).TotalMilliseconds;
			string input = "{\"actions\":[[{\"email\":\"" + chosenEmail + "\",\"time\":" + currentTs + ",\"area\":\"Water Saving Insights\",\"name\":\"Drop! correct answer\",\"description\":\"User inputs a correct answer playing the Drop!TheQuestion game\", \"tag\":\" \",\"link\":\" \",\"executor\":\" \"}]]}";
			
			Dictionary<string,string> headers = new Dictionary<string,string> ();
			headers.Add ("Content-Type", "application/json");
			
			byte[] body = Encoding.UTF8.GetBytes (input);
			
			WWW www = new WWW (geBasePath + geActionPath, body, headers);
			
			StaticCoroutine.DoCoroutine (WaitForRequest (www));
		}
	}
	
	public static void addRegistrationGe (string username) {
		if ("retrieving" == geBasePath && inetAvail) {
			retrieveGePath();
		}
		else
		if (username != null && username != "") {
			long currentTs = (long)(DateTime.UtcNow - new DateTime (1970, 1, 1)).TotalMilliseconds;
			string input = "{\"actions\":[[{\"email\":\"" + username + "\",\"time\":" + currentTs + ",\"area\":\"Water Saving Insights\",\"name\":\"Drop Download\",\"description\":\"The user register to the Drop game\", \"tag\":\" \",\"link\":\" \",\"executor\":\" \"}]]}";
			
			Dictionary<string,string> headers = new Dictionary<string,string> ();
			headers.Add ("Content-Type", "application/json");
			
			byte[] body = Encoding.UTF8.GetBytes (input);
			
			WWW www = new WWW (geBasePath + geActionPath, body, headers);
			
			StaticCoroutine.DoCoroutine (WaitForRequest (www));
		}
	}
	
	
	public static string getAdmin() { return adminUser; }
	
	public static string getAdminPass() { return adminPassword; }
	
}
