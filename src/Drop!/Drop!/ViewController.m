//
//  ViewController.m
//  Drop!
//
//  Created by Fausto Dassenno on 11/10/14.
//  Copyright (c) 2014 MSM. All rights reserved.
//

#import "ViewController.h"
#import "QRController.h"
#import "QuestionsRepo.h"

@interface ViewController ()

@end

@implementation ViewController

-(BOOL)prefersStatusBarHidden { return YES; }

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSDictionary* question = [QuestionsRepo getQuestion];
    
    UIImage *bg=[UIImage imageNamed:@"bg.png"];
    UIImageView* bg_i = [[UIImageView alloc] initWithImage:bg];
    bg_i.frame = CGRectMake(0, -self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:bg_i];
    
    UIView* cont=[[UIView alloc] initWithFrame:CGRectMake(-320, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.view.backgroundColor=[UIColor colorWithRed:152.0/255.0 green:195.0/255.0 blue:201.0/255.0 alpha:1];
    [self.view addSubview:cont];
    
    float top_start = 200;
    
    UIButton* single = [UIButton buttonWithType:UIButtonTypeCustom];
    [single setImage:[UIImage imageNamed:@"button_bg_sp.png"] forState:UIControlStateNormal];
    single.frame = CGRectMake(19, top_start, 282, 51);
    [single setTitle:@"Drop! The Game" forState:UIControlStateNormal];
    [cont addSubview:single];
    
    UIButton* wg_button = [UIButton buttonWithType:UIButtonTypeCustom];
    [wg_button setImage:[UIImage imageNamed:@"button_bg_wh.png"] forState:UIControlStateNormal];
    wg_button.frame = CGRectMake(19, top_start+62, 282, 51);
    [wg_button setTitle:@"Drop! The Game" forState:UIControlStateNormal];
    [cont addSubview:wg_button];
    
    UIButton* cg_button = [UIButton buttonWithType:UIButtonTypeCustom];
    [cg_button setImage:[UIImage imageNamed:@"button_bg_cg.png"] forState:UIControlStateNormal];
    cg_button.frame = CGRectMake(19, top_start+62+62, 282, 51);
    [cg_button setTitle:@"Drop! The Game" forState:UIControlStateNormal];
    [cg_button addTarget:self action:@selector(openQR) forControlEvents:UIControlEventTouchUpInside];
    [cont addSubview:cg_button];
    
    UIButton* set_button = [UIButton buttonWithType:UIButtonTypeCustom];
    [set_button setImage:[UIImage imageNamed:@"button_bg_sett.png"] forState:UIControlStateNormal];
    set_button.frame = CGRectMake(19, top_start+62+62+62, 282, 51);
    [set_button setTitle:@"Drop! The Game" forState:UIControlStateNormal];
    [cont addSubview:set_button];
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelay:1.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    bg_i.frame=self.view.frame;
    
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelay:2.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    cont.frame=self.view.frame;
    
    [UIView commitAnimations];
    
}

-(void)openQR {
    
    QRController* qr = [[QRController alloc] initWithFrame:self.view.frame];
    [self presentViewController:qr animated:NO completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
