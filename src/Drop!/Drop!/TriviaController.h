//
//  TriviaController.h
//  Drop!
//
//  Created by Fausto Dassenno on 13/10/14.
//  Copyright (c) 2014 MSM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TriviaController : UIViewController {
    
    NSDictionary* question;
    
}

-(id)initWithFrame:(CGRect)frame;

@end
