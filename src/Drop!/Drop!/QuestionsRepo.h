//
//  QuestionsRepo.h
//  Drop!
//
//  Created by Fausto Dassenno on 13/10/14.
//  Copyright (c) 2014 MSM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuestionsRepo : NSObject

+(NSDictionary*)getQuestion;

@end
